# NI-MVI Semester project

## Assignment

The goal of my semester project is to explore the Neural Networks architecture Wide & Deep (W&D) that was introduced by Google in 2016 as an architecture for items ranking in their recommendation system for Google Play. My work consists of paper research, finding examples of usage and evaluation on a selected dataset.

## Research

The original paper by Google \[1\] describes the W&D neural architecture in detail and proposes the implementation using Tensorflow. Google engineers used this architecture to boost the performance of application recommendations in Google Play and reported several percent increase in the app acquisition rate by almost 4%. NVIDIA \[2\] introduced another implementation of the W&D NN for NVIDIA GPUs and reported 11x faster online inference and 13x during offline testing.

The architecture became quite popular because of its simplicity and performance, and a lot of other resources exist that describe it from different angles, for example, Google blog post \[3\] about the architecture with the accompanying video \[4\]. The documentation of Tensorflow has a separate article \[5\] with the tutorial how to implement it using the `DNNLinearCombinedClassifier` class, Keras, on the other hand, has its own easy-to-use implementation \[6\]. Last but not least, there are implementations of the W&D NN using Pytorch \[7\].

## Architecture and motivation

As it was mentioned above, the main area where W&D models shine are recommendation systems, in particular, the ranking phase. Recall that one popular high-level architecture of recommendation systems is a two-phase pipeline. The first phase, *retrieval*, selects a subset of items that may be interesting to a user. On this stage, the main requirement is speed since we are dealing with millions of items and we want to quickly extract those that are somehow relevant. The second phase, ranking, is where we evaluate the probability of a user taking action (click, purchase, etc.) for every particular item. After we rank all items in the selected subset, we sort them in the descending order and serve $K$ first items. In this described scenario, the W&D NN is used to assign these probabilities.

The assignment task is not easy. We have to deal with two types of knowledge: generalized and memorized. From the original paper \[1\]:

> Memorization can be loosely defined as learning the frequent co-occurrence of items or features and exploiting the correlation available in the historical data. Generalization, on the other hand, is based on transitivity of correlation and explores new feature combinations that have never or rarely occurred in the past. Recommendations based on memorization are usually more topical and directly relevant to the items on which users have already performed actions. Compared with memorization, generalization tends to improve the diversity of the recommended items.

These concepts are covered by two most popular ML models. For memorization, generalized linear models are a great choice. Due to the simplicity they are easy to implement and are very fast. One can create millions of one-hot encoded cross-features and learn weights for those binary signals. These signals, therefore, act as toggles and a particular combination of these toggles describes an item (thus, memorization). However, due to the number of dimensions of such a feature space these models are in general bad in generalizing and are not capable of prediction for items not seen before. On the other hand, deep neural networks (DNN) create a representation of input features in a more compact latent space and because of this "compression" they are not able to distinguish perfectly between two similar objects. And this is exactly their generalization power since an unseen item, if it is similar to items seen before, can have a good representation for us to be able to use it for prediction.

The Wide & Deep architecture solves both problems by combining two models, linear and the DNN, into one and training both simultaneously. The linear (wide) part solves the memorization problem and the deep part generalizes. For the wide part we create a large number of binary cross-features and the input vector becomes very "wide". For the deep part, we create embedding representations of complex features (for example, text features), combine them into one smaller vector comparing to the wide one and feed it to the deep neural network. The output from both models is combined using the sigmoid function to get a probability. 


## Dataset

The dataset of my choice is the well-known MovieLens 1M Dataset \[8\]. The output unit of the W&D network allows us to use it for binary prediction tasks, however, the idea behind the architecture was fitted for recommendation problems. The dataset was contains information about 4000 movies, 6000 users and 1000000 interactions (ratings) between them. The main advantage of this dataset is that it contains not only the interactions but also additional information about users and movies which allows us to use these complex features for the deep part (generalization).

The predicted feature in the dataset is a film rating from one to five stars. Since the W&D network's output is binary, we need to somehow convert the output to a binary form. From the histogram of rating distribution in the dataset (see the notebook) we observe that items with ratings from one to three stars create 42.5% of the total mass of ratings. For the dataset to be balanced, we set the rule that films with ratings 1-3 will be labeled `0` and items with ratings 4-5 will be labeled `1`.

The dataset contains Zip codes of users and this information is too granular. With the help of the US Zip Codes Database \[9\], we will include the information about the state, the city and its population for almost every user.

The dataset is divided into train, validation and test set 8:1:1. Before the actual split, the dataset is sorted by the rating date column in the ascending order. Shuffling is disabled. The reason for that is because we don't want mix future and past ratings in one dataset. We train on the past and predict for the future.

## Implementation and results

The network is implemented in Keras. For the milestone, I managed to implement a pure deep model without the wide coomponent. I encoded one-hot categorical integer and string features usign TF Lookups. Next, I created a simple embedding for titles. User and Movie ID were also one-hot encoded. For the deep model alone these features are not descriptive, however, they will be used in the wide model for memorization purposes.

I tried several simple architectures which differed in the number of hidden layers and the number of neurons. Next, I manually iterated through several hypermarameters like the batch size, the embedding size, etc. I am not planning to implement an automated hypersearch since I don't have computational resources for such a task.

The model was trained on my Macbook Pro using the AMD Radeon Pro 5300M GPU. The network has 2M+ trainable parameters and showed 0.8 AUC on the training set, 0.77 AUC on the validation set. The inference on the test set on my configuration was terribly slow and I wasn't able to fix this. The results on the half of the test set are approx. 0.7 AUC.

## Next steps

First of all, I need to implement the wide component of the W&D network. I will use the same architecture for the deep NN, however, several categorical features will be one-hot encoded and used within the logistic part. In this way, I can create sparse binary cross-features and effectively use them for the memorization.

## References

- \[1\]: https://arxiv.org/abs/1606.07792
- \[2\]: https://developer.nvidia.com/blog/accelerating-wide-deep-recommender-inference-on-gpus/
- \[3\]: https://ai.googleblog.com/2016/06/wide-deep-learning-better-together-with.html
- \[4\]: https://www.youtube.com/watch?v=Xmw9SWJ0L50
- \[5\]: https://chromium.googlesource.com/external/github.com/tensorflow/tensorflow/+/r0.10/tensorflow/g3doc/tutorials/wide_and_deep/index.md
- \[6\]: https://www.tensorflow.org/api_docs/python/tf/keras/experimental/WideDeepModel
- \[7\]: https://www.kaggle.com/code/matanivanov/wide-deep-learning-for-recsys-with-pytorch
- \[8\]: https://grouplens.org/datasets/movielens/1m/
- \[9\]: https://simplemaps.com/data/us-zips
