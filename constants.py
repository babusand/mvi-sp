from pathlib import Path

# Roots

DATA_ROOT = Path("./data/")
RESOURCES_ROOT = Path("./resources/")
MODELS_ROOT = Path("./models/")

# Data paths

DATA_MOVIELENS_ROOT = DATA_ROOT / "ml-1m"
DATA_MOVIES = DATA_MOVIELENS_ROOT / "movies.dat"
DATA_USERS = DATA_MOVIELENS_ROOT / "users.dat"
DATA_RATINGS = DATA_MOVIELENS_ROOT / "ratings.dat"

DATA_USZIPS_ROOT = DATA_ROOT / "uszips"
DATA_ZIP = DATA_USZIPS_ROOT / "uszips.csv"

DATA_TRAIN = DATA_ROOT / "train.csv"
DATA_VAL = DATA_ROOT / "val.csv"
DATA_TEST = DATA_ROOT / "test.csv"

DATA_VOCAB = DATA_ROOT / "vocabulary.json"

# Features

FEATURE_DESC = DATA_ROOT / "fd.json"

# Models

BATCH_SIZE = 64
DEEP_LSIZE = [128, 32, 8, 4]

DROPOUT = 0.05
EPOCHS = 20
STEPS_EPOCH = 1024
VAL_STEPS = 64
WORKERS = 8

DEEP_LOAD_FROM_DISK = True

DEEP_IMG_PATH = RESOURCES_ROOT / "deep.png"
DEEP_MODEL_PATH = MODELS_ROOT / "deep.tf"

DW_IMG_PATH = RESOURCES_ROOT / "dw.png"
DW_MODEL_PATH = MODELS_ROOT / "dw.tf"
