# Wide & Deep Recommendations

**NI-MVI ZS 2022/23 - Semestral Work**  
*Bc. Andrey Babushkin* (babusand@fit.cvut.cz)

## Goals

Dive into the algorithm, research practical usages/examples, decrypt what “features” are typically used (explainability), evaluate performance on selected dataset.

## Data

The project requires tow datasets. The first is *MovieLens 1M* which can be found at grouplens.org/datasets/movielens/1m/. The dataset contains 1M ratings of 4000 movies by 6000 users. The dataset was release in 2003.

The second dataset is the "US Zip Codes Database", updated on 27.09.2022. It can be obtained at https://simplemaps.com/data/us-zips. The dataset contains geo information for US zip codes.

The data is stored in the `/models/` directory at can be found in two zip archives. Preprocessing and splitting is done in the `/01_data_preprocessing.ipynb` notebook, all steps are described.

## Modeling

We trained two models:

- Deep NN
- Wide & Deep NN

All code that was used to train and evaluate the models can be found in the `/02_wd.ipynb` notebook. All configuration contants can be found in `/constants.py`.

## Dependencies

Since Tensorflow was run on Macbook, the dependencies differ from non-macOS eenvironments. Instead of `tensorflow-metal`, one should install `tensorflow`. Other dependencies remain the same. The `conda-env.yml` file contains the complete virtual environment on which the code was run.
